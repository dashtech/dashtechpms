import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, EmailValidator } from '@angular/forms';
import { UserService } from '../../_services';
import { validateConfig } from '@angular/router/src/config';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html',
  providers: [UserService]
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  selectedFile: File;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private _userservice: UserService,
    private toastr: ToastrService
  ) { }
  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      UserName: ['', Validators.required],
      Email: ['', Validators.compose([Validators.required, Validators.email])],
      Password: ['', Validators.required],
      ConfirmPassword: ['', Validators.required],
      UserImage:['',Validators.required]
    });
  }
  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }
  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    console.log(this.selectedFile);
  }
  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      console.log(this.registerForm.invalid);
      return;
    }
    const uploadData = new FormData();
  uploadData.append('UserImages', this.selectedFile, this.selectedFile.name);
  uploadData.append('UserName',this.registerForm.value.UserName);
  uploadData.append('Email',this.registerForm.value.Email);
  uploadData.append('Password',this.registerForm.value.Password);
  uploadData.append('ConfirmPassword',this.registerForm.value.ConfirmPassword);
  this._userservice.register(uploadData)
      .subscribe(response => {
        //console.log('Respnse Msg:' + response);
        this.toastr.success('Welcome '+this.registerForm.value.Email); 
        window.location.replace("/#/flogin");
      },
        error =>{//console.error('Error Msg' + error)
        if (error && error.error) {
          debugger
          this.toastr.error(error.error.message, "Invalid !");
        }
      }
      );
  }

}
