import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService, UserService } from '../../_services';
import { AlertService } from '../../_helper';
import { Login } from '../../_model';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html',
  providers: [UserService, AuthenticationService]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  private _login: Login; 
  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private _alertService: AlertService,
    private toastr: ToastrService,
    private _userservice: UserService) {

  }
  ngOnInit() {

    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    // reset login status
    this.authenticationService.logout();
    //this._alertService.error('Something Wrong here..');
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }

    this.authenticationService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        resp => {debugger
          if (resp && resp.access_token) {
            //this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
            console.log(resp);
            //localStorage.setItem('pmsuser', JSON.stringify(resp));
            if (this.returnUrl !== '/') {
              this.router.navigate([this.returnUrl]);
            }
            else {
              this.router.navigate(['/dashboard']);
            }
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            var a = this._userservice.getById(1);
            this._login=resp;
            localStorage.setItem('pmsuser', JSON.stringify(this._login));
            this.toastr.success('Welcome '+this._login.displayname);
          }
        },
        error => {
          debugger
          if (error && error.error) {
            this.toastr.error(error.error.message, "Invalid !");
          }
        });

  }

  forgetpass() {
    window.location.replace("/#/forgetpassword");
  }
  register() {
    window.location.replace("/#/register");
  }

}
