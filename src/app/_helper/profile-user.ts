import { Component, OnInit, Injectable } from '@angular/core';
import { LoginResponse } from '../_model';

@Injectable()
export class UserProfile {

    private userprofile: LoginResponse;
    constructor() { }

    getuserprofile():LoginResponse {
        if (localStorage.getItem('currentUser')) {
            return this.userprofile = JSON.parse(localStorage.getItem('currentUser'));
        }
    }
    // getuserid() {
    //     if (localStorage.getItem('currentUser')) {
    //         this.userprofile = JSON.parse(localStorage.getItem('currentUser'));
    //         return this.userprofile.userid;
    //     }
    // }
    getusername() {
        if (localStorage.getItem('currentUser')) {
            this.userprofile = JSON.parse(localStorage.getItem('currentUser'));
            return this.userprofile.displayname;
        }
    }

    

}