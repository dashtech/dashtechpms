import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Response } from '@angular/http';
import { map } from 'rxjs/operators';
import { UserService } from './user.service';
import { UserLogin } from '../_model';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class AuthenticationService {
    private _userlogin: UserLogin;
    //private _token: string = "http://localhost:59327/token";
    private _token: string = "http://localhost:59327/api/Account/Login";
    headers: HttpHeaders;
    options: RequestOptions;
    constructor(private http: HttpClient, private _userservice: UserService) {
    }


    login(username: string, password: string): Observable<any> {
        return this.http.post(this._token,{UserName:username,Password:password});
    }

    logout() {
        localStorage.removeItem('pmsuser');
    }
}

