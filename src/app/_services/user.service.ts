import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserDetail } from '../_model';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class UserService {
    private _url_post_regi: string = "http://localhost:59327/api/Account/Register";
    

    constructor(private http: HttpClient) { }

    getAll() {
        //return this.http.get<User[]>(`/users`);
        //return this.user;
    }

    getById(id: number) {
        return this.http.get(`/users/` + id);
        //return this.user.find(f => f.Id == id);
    }

    register(user: UserDetail) {
        return this.http.post(this._url_post_regi, user);
    }

    update(user: UserDetail) {
        return this.http.put(`/users/` + user.UserId, user);
    }

    delete(UserId: number) {
        return this.http.delete(`/users/` + UserId);
    }
}