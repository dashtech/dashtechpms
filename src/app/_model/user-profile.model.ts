export class UserDetail {
    UserId: number;
    UserName: string;
    Email: string;
    Password: string;
    ConfirmPassword: string;
    UserImage:File;
    //firstName: string;
    //lastName: string;
}

export class LoginResponse {
    token_type: string;
    access_token: string;
    userid: string;
    displayname: string;
    profileimg: string;
}

export class UserLogin {
    UserName: string;
    Password: string;
}